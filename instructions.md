# Introduction

As before, you will submit this assignment via Git and #githost. This Analysis assignment tells you what to submit (#filename(analysis.R)), and how to do it (see “Instructions” below).

IMPORTANT: You MUST name your file #filename(analysis.R) exactly as shown, with NO SPACES, all lower case letters, and a capital #filename(.R) extension. Failure to do so will mean our marking script will not find your file, and you will receive a mark of zero (0) for this assignment.

_Pay attention!_ If you do it wrong, you’ll get a mark of zero (0), which will substantially reduce your chances of passing this module.

# Instructions

1. Update #filename(research_question.yml)

    If you have changed either your research question, or your dataset, since the Research Question assignment, you should update #filename(research_question.yml) to reflect these changes.

    If you have changed your dataset, you need to remove the old dataset, and add (and commit) your new dataset, as a CSV file.

2. _Validate_ your #filename(research_question.yml) file, using the
following R command:
#command(yaml::read_yaml("research_question.yml")).  Be sure that the
command executes successfully, and that _all_ fields are populated
correctly.  You can run #command(Rscript validate_yaml.R) using the
#filename(validate_yaml.R) script in this repository to test your #filename(research_question.yml).

    We use the contents of #filename(research_question.yml) to
    interpret your analysis, so you must ensure it is
    correct and up-to-date.  Invalid YAML files will result in a
    failing grade for the analysis deliverable.

3. Update and _test_ your #filename(visualization.R) file, using the
the `Rscript`  command from the Windows command line:

        Rscript visualization.R

    Be sure that the command executes successfully, and that it
produces a #filename(visualization.pdf) containing a histogram of the
dependent variable if your research question is about correlation or
comparison of means/medians.

    We use the contents of #filename(visualization.pdf) to assess
whether your analysis uses the correct test statistic, so you must
ensure it is correct and up-to-date.


4. Create an R script called #filename(analysis.R)

    Using Notepad++ (or another text editor of your choice), or R Studio,
create a file called #filename(analysis.R) in the Git workspace
for your repository.

    IMPORTANT: _You MUST name your file #filename(analysis.R) exactly as shown, with NO SPACES, all lower case text, and a capital #filename(.R) extension. Failure to do so will mean our marking script will not find your file, and you will receive a mark of zero (0) for this assignment._

    Your script should do the following:

    1. Load any required libraries. Be sure these are part of _tidyverse_ or base R; anything else will cause your script to fail, and you will receive zero (0) credit for this assignment.
    
    2. Load your dataset from the _current working directory_.  Do not, under any circumstances, hard code absolute paths into your script: this will guarantee that your script will fail execution, and you will receive a mark of zero (0) for this assignment.
    
    3. Run the appropriate statistical test for your data and research
    question (see below), and let it _print it's output to
    #filename(stdout)_^[Just call the test function, it will print to #filename(stdout).  Please do not print anything else.];
    see the example #filename(analysis.R) to see how this works.

5. Commit #filename(analysis.R) to your Git repository workspace.

6. Test #filename(analysis.R) to be sure it does what it’s meant
to do:  from the _command line_, execute:

        Rscript analysis.R

    The final output should be from your statistical test, nothing else.

7. Use #command(git rm) to remove any extraneous files (editor backup files, old experiments, your old dataset if you changed datasets, and #filename(visualization.pdf)) if you committed it mistakenly (never commit derived files: they get overwritten the very next time you run a script, and so they often create spurious merge conflicts).

8. Push your clean, up-to-date, working files to BitBucket by 23:59 on #cw_analysis_due.

9. Have one of your teammates clone your repository and test
#filename(analysis.R) to be sure it does what it’s meant to do,
from a _different computer_.

    Make any necessary changes to fix problems, then commit, push, and
    test again.

# What is an "appropriate" test for your RQ and data?


![Analysis decision tree](analysis_flowchart.pdf){ width=100% }


## Correlation

If your research question asks about correlation, the appropriate test
is #command(cor.test), with either "pearson," "kendall," or "spearman"
as the _method_; use:

* `method="pearson"` if your data appear to be normally distributed
  (look at your histogram and overlayed normal curve to determine
  whether your data look normally distributed).

* `method="spearman"` or `method=kendall"` if your data do not appear
  to be normally distributed, or your dependent variable is _ordinal._
  
## Difference in means.

* If your research question asks about difference in means, and your
data appear to be normally distributed (look at your histogram and
overlayed normal curve to determine whether your data look normally
distributed), the appropriate test is #command(t.test).

    Note:  #command(t.test) only works with _two_ categories; if your
    data have more than two categories, use
    #command(pairwise.t.test).
    
* If your research question asks about difference in means, and your
data do not appear to be normally distributed, or your dependent
variable is _ordinal_, the appropriate test is
#command(wilcox.test) with _two samples_ (this is also known as the
Mann-Whitney U test, but is called #command(wilcox.test) in R).

    Note:  #command(wilcox.test) only works with _one_ or _two_ categories; if your
    data have more than two categories, use
    #command(pairwise.wilcox.test).  This will compare all groups to
    each other in a pairwise fashion.

Do **NOT** use ANOVA or #command(pairwise_t_test)!

## Difference in proportions

If your research question asks about difference in proportions, the
appropriate test is #command(chisq.test).








# Assessment criteria

Your #filename(analysis.R) will be assessed on two major criteria:

1. Does it work?

    * Does the script load only libraries from _tidyverse_ or base R?
    * Does the script load the dataset from the _current working directory?_
    * Does the script print the analysis results to the _standard output?_

2. Is it correct?

    * Does the script create the appropriate analysis for the research
      question and associated variables?
    * If parametric statistics are used, does the histogram plot of
      the dependent variable suggest the data are normally distributed?

